---
title: Ongi etorri!
---

{{< expand "Aurkibidea" "..." >}}
- [Kontaktatu](#kontaktatu)
- [Nor gara](#nor-gara)
- [Fedibertsoa](#fedibertsoa)
{{< /expand >}}

**[Lemmy.eus](https://lemmy.eus/) euskarazko web-foro irekiak** dira. Webgune hau *Lemmy* deitzen den software libre eta federatu baten euskarazko instantzia bat da. Munduan badira hizkuntza eta komunitate desberdinetara zuzendutako instantziak eta, ia etengabe, sorpresa berriak agertzen dira. Hau euskarazko lehen Lemmy instantzia bada ere, laster gehiago izatea primeran legoke! [lemmy.eus](https://lemmy.eus/)-en albisteak, bitxikeriak, nori galdetu ez dakizun hori, umorea, eztabaidak, hausnarketa sakonak, meme alprojak... eta auskalo zer gehiago aurkituko duzu. Webguneko erabiltzaileon esku dago zein gairi buruz hitz egin eta zein informazio partekatu, lemitarrok dugu hitza eta erabakia :-)

Webgune honen logotipoa, softwarearen berdina da:

![Logotipoa](/lemmy-300px.png)

Gure [netiketa arauak](/edukiak/netiketa) irakurtzera animatzen zaitugu eta gero, nahi baduzu, [lemmy.eus](https://lemmy.eus/)-en zure erabiltzailea sortzera. Gidatxo honetan azaltzen dizugu [nola eman izena](/edukiak/hasi) eta nola harpidetu zure gustuko komunitateetara. Landu nahi duzun gai bat aurkitzen ez baduzu, beti duzu aukera zure komunitatea sortzeko eta dinamizatzeko! Animatu eta parte hartu!

## Kontaktatu

Administratzaileekin harremanetan jartzeko **[info@lemmy.eus](mailto:info@lemmy.eus)** epostara idatzi.

Lemitarron komunitateak txat bat ere badauka! Txat bakar horretan bi plataforma desberdinetik parte har dezakezu:
- *Matrix* plataforma libreko [#lemmyeus_komunitatea](https://matrix.to/#/#lemmyeus_komunitatea:sindominio.net) gelatik.
- *Telegram* plataforma pribatuko [@lemmyeus_komunitatea](https://t.me/lemmyeus_komunitatea) taldetik.

Sartu bietako edozeinetan eta hasi webguneko beste zenbait erabiltzailerekin hizketan :-)

## Nor gara

[Lemmy.eus](https://lemmy.eus/) martxan jarri dugunak, software librearen eta fedibertsoaren alde gauden euskaldun talde txikitxo bat gara. Esparru horretan jardutearen eraginez ezagutzen gara. Euskarazko *Lemmy*, *Peertube*, *Nextcloud*... instantzia gehiago egotea nahi genuen eta helburu horrekin **Abaraska** taldea sortu genuen. Lemmy instantzia honetaz gain, baliteke etorkizunean webgune gehiago martxan jartzea. Zu ere gogoz bazaude, jar zaitez [gurekin harremanetan](#kontaktatu)!

## Fedibertsoa

[Fedibertsoa](https://eu.wikipedia.org/wiki/Fedibertso), *'federazio'* eta *'unibertso'* hitzen konbinaziotik dator eta modu federatuan eta irekian web edukiak argitaratzen dituzten ostalariei deitzeko erabiltzen da. Independenteak izan arren, protokolo estandar berdinak erabiliz (ActivityPub, OStatus...), *instantzia* bateko erabiltzaileek (hala deitu ohi dira zerbitzariak), beste instantzia bateko erabiltzaileekin edo beraiek argitaratutako edukiekin eragiketak burutu ditzakete: jarraitu, mezua bidali, iruzkindu... Askotariko sare horien baturari fedibertsoa deitzen zaio eta logotipo hau du:

![Fedibertsoko logotipoa](/fedibertsoa-300px.png)

Euskal fedibertsoa osatzen duten webguneak ondokoak dira:

| Webgunea | Softwarea | Eduki-mota| Izen emate<br />irekia |
| :--- | :---: | :---: | :---: |
| [agenda.eskoria.eus](https://agenda.eskoria.eus/) | Gancio | Agendak | Bai |
| [baleafunk.eus](https://baleafunk.eus) | Funkwhale | Audioak | Bai |
| [bideoak.argia.eus](https://bideoak.argia.eus/) | Peertube | Bideoak |  Ez |
| [bilbi.info](https://bilbi.info/) | Gancio | Agendak | Bai |
| [hegolapurdi.euskaragendak.eus](https://hegolapurdi.euskaragendak.eus/) | Gancio | Agendak | Bai |
| [lemmy.eus](https://lemmy.eus/) | Lemmy | Foroak | Bai |
| [lubakiagenda.net](https://lubakiagenda.net/) | Gancio | Agendak | Bai |
| [mastodon.eus](https://mastodon.eus/) | Mastodon | Mikroblogak | Bai |
| [mastodon.jalgi.eus](https://mastodon.jalgi.eus/) | Mastodon | Mikroblogak | Bai |
| [paperjale.eus](https://paperjale.eus/) | BookWyrm | Liburuak |  Bai |
| [peertube.eus](https://peertube.eus/) | Peertube | Bideoak |  Bai |
| [peertube.euskarabildua.eus](https://peertube.euskarabildua.eus/) | Peertube | Bideoak |  Ez |
| [pixelfed.eus](https://pixelfed.eus/) | Pixelfed | Argazkiak | Bai |

Euskarazko edukietara zuzenduta dagoen fedibertsoko beste instantziarik zerrenda honetara gehitzea nahi baduzu, jar zaitez gurekin harremanetan mesedez :-)

Software-mota eta instantzia askoz gehiago daude. Informazio gehiago aurkituko duzu [Wikipedian](https://eu.wikipedia.org/wiki/Fedibertso) eta [fediverse.party](https://fediverse.party/) webgunean.

{{< hint info >}}
Lemmy softwarea ActivityPub protokoloaren bitartez beste Lemmy instantziekin federatzeko gai da. Dagoeneko [lemmy.eus](https://lemmy.eus/) instantzia beste batzuekin federatzen hasi da. Hemen ikus daiteke zeintzuekin: https://lemmy.eus/instances.

Lemmy instantzia bat sortzekotan bazaude eta laguntzarik behar baduzu, jar zaitez [gurekin harremanetan](#kontaktatu)!
{{< /hint >}}
