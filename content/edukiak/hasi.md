---
title: "Lehen urratsak"
weight: 10
draft: false
---

{{< expand "Aurkibidea" "..." >}}
- [1. Izena eman](#izena-eman)
- [2. Komunitateetara harpidetu](#komunitateetara-harpidetu)
- [3. Zure ezarpenak doitu](#zure-ezarpenak-doitu)
{{< /expand >}}

Lemmy.eus webguneko informazioa publikoa da. Bere web-orri desberdinetatik nabigatu dezakezu eta zuk nahi beste ikusi eta irakurri!

Webguneari benetako zukua ateratzeko (komunitateetara harpidetu, edukiak gehitu, bozkatu edo iruzkindu...), webgunean izena eman behar duzu.

## 1. Izena eman {#izena-eman}

Webgunean izena emateko edo erregistratzeko, eskuinean goian agertzen den botoi urdin hau sakatu behar duzu:

> ![Hasi saioa / Eman izena](/hasi-saioa-eman-izena.png)

Hori egitean, bi formulario agertzen zaizkizu: *"Hasi saioa"* eta *"Eman izena"*. Oraindik izena eman ez duzunez, hau da bete behar duzun formularioa:

> ![Eman izena formularioa](/eman-izena.png "Hau irudi bat da")

- Sartu Lemmy.eus-en izan nahi dituzun *"Erabiltzaile-izena"* eta *"Pasahitza"*. Pasahitza birritan sartu behar duzu eta gutxienez 8 karaktereko luzera izatea gomendatzen dizugu, hizkiak, zenbakiak eta karaktere bereziren bat konbinatuz.
- Nahi baduzu, zure *"Eposta"* ere sar dezakezu. Zure Lemmy.eus-eko erabiltzaileak eposta helbide bat esleituta izateak abantaila batzuk ditu, besteak beste, jakinarazpenak epostaz ere jasotzeko aukera izango duzula. Epostarik jartzen ez baduzu, etorkizunean zure pasahitza ahazten baduzu, ezingo duzu zure pasahitza berrezartzeko epostarik eskatu.
- *"Sartu kodea"* eremuan, iruditxoan agertzen diren hizkiak sartu behar dituzu.
- Hunkigarria izan daitekeen edozein eduki zuk espresuki eskatu ezean bistaratzerik nahi ez baduzu (hizkera desegokia, erotismoa...), *"eduki hunkigarriak (NSFW)"* marka aktibatzea gomendatzen dizugu. Zure erabiltzailearen ezarpenetan, beti izango duzu aukera hau eta beste batzuk aldatzeko aukera.

Formularioa betetakoan, *"Eman izena"* botoi berdea sakatu. Zuzendu beharreko zerbait badago, jarraibideak ikusiko dituzu.

## 2. Komunitateetara harpidetu {#komunitateetara-harpidetu}

Ondorengo urratsa, interesatzen zaizkizun komunitateetara harpidetzea da. Komunitate bakoitza foro independentea da. Batzuk zure interesekoak izan daitezke eta beste batzuk berriz ez. Zure gustukoetara harpidetzen bazara, Lemmy.eus bisitatzen duzun bakoitzean zure komunitateetako edukiak bakarrik ikusi ahal izango dituzu (oso praktikoa) edo eduki berri guztiak.

> ![Komunitateen zerrenda](/komunitate-zerrenda.png "Hau irudi bat da")

Komunitateetara harpidetzeko, dagozkien *"Harpidetu"* estekak sakatu besterik ez duzu. Nahi duzun komunitate guztietara harpidetu ondoren, *"Hurrengoa"* botoia sakatu.

## 3. Zure ezarpenak doitu {#zure-ezarpenak-doitu}

Orain arte egindakoa nahikoa izan badaiteke ere, zure lehen urratsak burutzeko gauza bat gehiago egitea gomendatzen dizugu: sortu berri duzun erabiltzailearen ezarpenak doitzea edo osatzea. Horretarako, eskuinean goian aukeratu duzun erabiltzaile-izena agertzen dela ikusiko duzu. Bertan sakatu eta *"Ezarpenak"* kutxan, aldatu ditzakezun hainbat eremu daudela ikusiko duzu:

> ![Ezarpenak formularioa](/erabiltzaile-ezarpenak.png "Hau irudi bat da")

- *"Avatarra"*, erabiltzailearen iruditxoa da. Fitxategia igoz gero, zure izena agertzen den bakoitzean, ondoan iruditxo hori bistaratuko da.
- *"Itxura"*, webgunearen itxura aldatzeko erabil dezakezu. Adibidez, *"darkly"* itxura hautatuz gero, lehenetsita datorren atzealde zuria ikusi ordez atzealde beltza ikusiko duzu.
- *"Biografia"* eremuan nor zaren azaldu dezakezu: nongoa zaren, zer duzun gustuko...
- Zure pasahitza aldatu nahi baduzu, formulario honetan egin dezakezu.
- Eduki hunkigarriak (NSFW) automatikoki bistaratzea nahi baduzu, zuri galdetu gabe, dagokion kontrol-kutxa gaitu behar duzu. Desgaituta badago, etiketatutako eduki hunkigarri guztiak babestuta agertuko zaizkizu eta ez dituzu lehen unean ikusiko.
- Webgunean jasotzen dituzun jakinarazpenak epostaz ere jaso nahi badituzu (norbaitek zure bidalketa bat erantzuten badu, mezu zuzen bat jasotzen baduzu...), *"Bidali jakinarazpenak epostara"* kontrol-kutxa gaitu behar duzu. Desgaituta badago, ez duzu epostarik jasoko eta jakinarazpenak ikusteko lemmy.eus webgunean sartu eta saio hasi beharko duzu.

Eta kito, honekin nahikoa eta sobera. Galdera gehiago badituzu, ikusi [ohiko galderak](/edukiak/ohiko-galderak/) atala. Bestela, zoaz [lemmy.eus](https://lemmy.eus/) webgunera eta lemitarra bihur zaitez!
